export interface IDinosaur {
  id: number
  name: string
  img: string
  type: string
  length: string
  diet: string
  description: string
  age: string
  taxonomy: string[]
  selected?: boolean
}