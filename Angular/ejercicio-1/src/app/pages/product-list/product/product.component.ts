import { Component, Input, Output, EventEmitter } from '@angular/core';
import { IDinosaur } from '../models/product.model';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss'],
})
export class ProductComponent {
  @Input() public product?: IDinosaur;

  @Output() public select: EventEmitter<void> = new EventEmitter();

  constructor() {}

  public onSelect(): void {
    if (this.product) {
      // cambio la propiedad de seleccinado
      this.product.selected = !this.product.selected;
      // emito el evento para updatear la visualizacion
      this.select.emit();
    }
  }
}
