import { IDinosaur } from './models/product.model';
import { productData } from './product-list.config';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss'],
})
export class ProductListComponent implements OnInit {
  public productList: IDinosaur[] = productData;
  public productListFiltered: IDinosaur[] = productData;
  public selectedItemsCount: number = 0;
  public textCount: string = '';
  public isFilter: boolean = false;

  constructor() {}

  ngOnInit(): void {
    this.generateCountText();
  }

  public onSelect(): void {
    //updated selected count
    this.updateSelectedCount();
    //update text
    this.generateCountText();
    //update display
    this.updateDisplay();
  }

  public updateSelectedCount(): void {
    this.selectedItemsCount = this.productList.filter(
      (product) => product?.selected
    ).length;
  }

  public updateDisplay(): void {
    if (this.selectedItemsCount === 0) {
      // si no hay elementos seleccionados, reseteo el product list
      // y reseteo el valor de filter
      this.productListFiltered = [...this.productList];
      this.isFilter = false;
      return;
    }
    if (this.isFilter) {
      this.productListFiltered = this.productList.filter(
        (product) => product.selected
      );
    } else {
      this.productListFiltered = [...this.productList];
    }
  }

  public generateCountText(): void {
    if (this.selectedItemsCount <= 0)
      this.textCount = 'No hay elementos seleccionados';
    else if (this.selectedItemsCount === 1)
      this.textCount = '1 elemento seleccionado';
    else this.textCount = `${this.selectedItemsCount} elementos seleccionados`;
  }

  public toggleFilter(): void {
    this.isFilter = !this.isFilter;
    this.updateDisplay();
  }
}
