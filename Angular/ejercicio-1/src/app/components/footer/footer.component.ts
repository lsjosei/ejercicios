import { socialMedia } from './models/social-media.model';
import { Component } from '@angular/core';
import { socialMediaInfo } from './footer.config';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent  {

  public socialMedia?: socialMedia[] = socialMediaInfo;

  constructor() { }
}
