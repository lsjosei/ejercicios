import { socialMedia } from './models/social-media.model';
export const socialMediaInfo: socialMedia[] = [
  {
    name: 'Twitter',
    url: 'https://www.twitter.com',
    image: 'https://cdn1.iconfinder.com/data/icons/social-media-rounded-corners/512/Rounded_Twitter5_svg-512.png',
  },
  {
    name: 'Facebook',
    url: 'https://www.facebook.com',
    image: 'https://cdn1.iconfinder.com/data/icons/social-media-rounded-corners/512/Rounded_Facebook_svg-512.png',
  },
  {
    name: 'Instagram',
    url: 'https://www.instagram.com',
    image: 'https://cdn1.iconfinder.com/data/icons/social-media-rounded-corners/512/Rounded_Instagram_svg-512.png',
  },
];
