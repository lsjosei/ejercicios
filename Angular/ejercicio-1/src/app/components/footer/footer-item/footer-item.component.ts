import { Component, Input } from '@angular/core';
import { socialMedia } from '../models/social-media.model';

@Component({
  selector: 'app-footer-item',
  templateUrl: './footer-item.component.html',
  styleUrls: ['./footer-item.component.scss']
})
export class FooterItemComponent  {

  @Input() public item?: socialMedia;
  constructor() { }


}
