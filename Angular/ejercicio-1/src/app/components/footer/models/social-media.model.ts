export interface socialMedia {
  name: string,
  url: string,
  image: string
}