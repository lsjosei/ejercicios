import { IGame } from './../shared.config';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterList',
})
export class FilterListPipe implements PipeTransform {
  transform(
    list: IGame[],
    currentPage: number = 1,
    itemsPerPage: number = 10
  ): IGame[] {
    return list.slice(
      (currentPage-1) * itemsPerPage,
      (currentPage) * itemsPerPage
    )
  }
}
