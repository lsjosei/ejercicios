import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {
  public mode: string = ''; // controls which links are displayed

  public routes: string[] = ['', 'list', 'contact'];

  public routeNames = new Map<string, string>();

  constructor(private router: Router) {
    this.routeNames.set('', 'Home');
    this.routeNames.set('list', 'List');
    this.routeNames.set('contact', 'Contact');
  }

  ngOnInit(): void {
    this.router.events.subscribe((e) => {
      if (e instanceof NavigationEnd) {
        this.mode = this.router.url.split('/')[1];
      }
    });
  }
}
