import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path:"",
    pathMatch:"full",
    loadChildren: () => import("./pages/home/home.module").then(m => m.HomeModule)
  },
  {
    path:"list",
    loadChildren: () => import("./pages/list/list.module").then(m => m.ListModule)
  },
  {
    path:"detail/:id",
    loadChildren: () => import("./pages/detail/detail.module").then(m => m.DetailModule)
  },
  {
    path:"contact",
    loadChildren: () => import("./pages/contact/contact.module").then(m => m.ContactModule)
  },
  {
    path:"**",
    redirectTo:""
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
