import { IGame, data } from 'src/app/shared/shared.config';
import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss'],
})
export class DetailComponent implements OnInit {
  public id: number = 0;

  public data: IGame[] = data;

  public game?: IGame;

  constructor(private route: ActivatedRoute, private router: Router) {}

  ngOnInit(): void {
    const id: string = this.route.snapshot.paramMap.get('id') ?? '0';
    this.game = data.find(ele => ele.id === id)
    if (!this.game) {
      this.router.navigateByUrl("/list")
    }
  }
}
