import { Component, OnInit } from '@angular/core';
import { IGame, data } from 'src/app/shared/shared.config';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent implements OnInit {
  public currentPage: number = 1;
  public data: IGame[] = data;
  public itemsPerPage: number = 4;

  constructor() {}

  ngOnInit(): void {}

  public decreasePage(): void {
    if (this.currentPage > 1) this.currentPage--;
  }
  public increasePage(): void {
    if (this.currentPage * this.itemsPerPage < data.length) this.currentPage++;
  }
}
