// Creamos una enumeración para limitar las operaciones existentes en nuestra calculadora.
enum Operacion {
  SUMA = "+",
  RESTA = "-",
  DIVISION = "/",
  MULTIPLICACION = "*",
  POTENCIA = "^",
}

const operando1: number = parseInt(process.argv[2])
const operando2: number = parseInt(process.argv[3])
/** Casteo al tipo Operacion para indicarle que el valor que introduce el usuario por consola es de dicho tipo */
const operacion: Operacion = process.argv[4] as Operacion

/** Creamos una función que raliza la operación que le especifiquemos y devuelve el resultado.
 * Si la operació no existe devuelve -1
 */
const operar = (
  operando1: number,
  operando2: number,
  operacion: Operacion
): number => {
  switch (operacion) {
    case Operacion.SUMA:
      return sumar(operando1, operando2)
    case Operacion.RESTA:
      return restar(operando1, operando2)
    case Operacion.DIVISION:
      return dividir(operando1, operando2)
    case Operacion.MULTIPLICACION:
      return multiplicar(operando1, operando2)
    case Operacion.POTENCIA:
      return potencia(operando1, operando2)
    default:
      console.log("ERROR: La operación introducida no existe")
      return -1
  }
}

/** Función que suma dos números */
const sumar = (operando1: number, operando2: number) => {
  return operando1 + operando2
}

const restar = (operando1: number, operando2: number) => {
  return operando1 - operando2
}

const dividir = (operando1: number, operando2: number) => {
  if (operando2 === 0) {
    console.log("ERROR: División entre 0")
    return -1
  }
  return operando1 / operando2
}

const multiplicar = (operando1: number, operando2: number) => {
  return operando1 * operando2
}

const potencia = (operando1: number, operando2: number) => {
  return Math.pow(operando1, operando2)
}

/** Se infiere el tipo number de la función al ser el tipo de su valor de retorno */
const result = operar(operando1, operando2, operacion)
/** Imprimimos el resultado de nuestra operación */
console.log(result)
