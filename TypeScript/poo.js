/** Crear interfaces y clases para definir esta situación:

Tenemos una ciudad con varias CASAS , estas pueden ser compradas por distintas PERSONAS.
 De las casas necesitamos guardar la información de su superficie, precio,
 número de habitaciones, número de baños, tipo de casa (chalet, piso o duplex),
 si la casa está en venta, y su propietario/s en caso de que los tenga.
 De las personas necesitamos conocer su nombre, su edad, el dinero del que
 disponen, su DNI y su estado civil,
 en caso de que las personas estén casadas además necesitamos conocer su pareja.

Habrá 2 acciones que sea posible realizar:

- Comprar una casa: En caso de que una casa sea adquirida deberá dejar
de estar en venta y se deberá registrar su propietario o propietarios.
- Casarse: 2 personas podrán casarse si no están ya casados con otra
persona. Se deberá registrar su nueva pareja y cambiar su estado civil.

Se deberán crear las clases e interfaces necesarias para poder definir
las distintas personas y casas.
Además las clases deberán contener los métodos necesarios para que se
puedan realizar las acciones descritas.
Se deberá intentar utilizar tipos personalizados y herencia de clases. */
/** Enumeración con los tipos de casa, completar */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __spreadArray = (this && this.__spreadArray) || function (to, from, pack) {
    if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
        if (ar || !(i in from)) {
            if (!ar) ar = Array.prototype.slice.call(from, 0, i);
            ar[i] = from[i];
        }
    }
    return to.concat(ar || Array.prototype.slice.call(from));
};
var TipoCasa;
(function (TipoCasa) {
    TipoCasa["CHALET"] = "chalet";
    TipoCasa["PISO"] = "piso";
    TipoCasa["DUPLEX"] = "duplex";
})(TipoCasa || (TipoCasa = {}));
/** Enumeración con los tipos de estado civil, completar */
var EstadoCivil;
(function (EstadoCivil) {
    EstadoCivil["CASADO"] = "casado";
    EstadoCivil["SOLTERO"] = "soltero";
})(EstadoCivil || (EstadoCivil = {}));
var Propiedad = /** @class */ (function () {
    function Propiedad(precio, superficie) {
        this.precio = precio;
        this.superficie = superficie;
        this.enVenta = true;
        this.propietarios = [];
    }
    Propiedad.prototype.comprar = function (compradores) {
        // Si está en venta se permite comprarla (añadir condición para que los compradores tengan el dinero)
        // y restar de su dinero el precio de la casa si la compran.
        var capitalCompradores = compradores.reduce(function (a, b) { return b.dinero + a; }, 0);
        var isBudget = capitalCompradores >= this.precio;
        if (!this.enVenta)
            console.log("ERROR: No esta en venta");
        else if (!isBudget)
            console.log("ERROR: Fondos insuficientes");
        else {
            // Almaceno los antiguos propietarios
            var antiguosPropietarios = __spreadArray([], this.propietarios, true);
            // Se actualizan propietarios
            this.propietarios = __spreadArray([], compradores, true);
            // La casa deja de estar en venta.
            this.enVenta = false;
            // Genero un mensaje de respuesta
            var nombresCompradores = compradores.map(function (ele) { return ele.nombre; });
            console.log("".concat(nombresCompradores.join(", "), " han comprado una propiedad"));
            // Cobro el dinero a los nuevos propietarios
            var deuda_1 = this.precio;
            compradores.forEach(function (comprador) {
                // Por desgracia voy dejando sin dinero por orden
                // a los propietarios hasta que la deuda esta pagada
                if (comprador.dinero <= deuda_1) {
                    deuda_1 -= comprador.dinero;
                    comprador.dinero = 0;
                }
                else {
                    comprador.dinero -= deuda_1;
                    deuda_1 = 0;
                }
            });
            // El primero antiguo propietario recibe todo el dinero de la compra
            // ya luego hace cuentas con los demas
            if (antiguosPropietarios.length > 0)
                antiguosPropietarios[0].dinero += this.precio;
        }
    };
    return Propiedad;
}());
/** Clase persona, completar con atributos y constructor */
var Persona = /** @class */ (function () {
    function Persona(nombre, edad, dinero, dni, estadoCivil) {
        this.nombre = nombre;
        this.estadoCivil = estadoCivil;
        this.edad = edad;
        this.dinero = dinero;
        this.dni = dni;
    }
    /** Implementar lógica para actualizar el estado civil de ambas personas y su pareja
     * Se deberá comprobar que las 2 personas estén solteras antes de casarlos.
     */
    Persona.prototype.casarse = function (persona) {
        if (this.edad < 18 || persona.edad < 18) {
            // Comprobacion de mayoria de edad
            console.log("ERROR: ambas personas deben ser mayores de edad");
        }
        else if (this.estadoCivil !== "soltero" ||
            persona.estadoCivil !== "soltero") {
            // Comprobacion de ambos solteros/as
            console.log("ERROR: ambas personas deben estar solteras");
        }
        else {
            // Actualizo estado civil
            this.estadoCivil = EstadoCivil.CASADO;
            persona.estadoCivil = EstadoCivil.CASADO;
            // Actualizo referencia a pareja
            this.pareja = persona;
            persona.pareja = this;
            // Genero mensaje de respuesta
            console.log("".concat(this.nombre, " y ").concat(persona.nombre, " se han casado"));
        }
    };
    return Persona;
}());
/** Clase casa, completar con atributos que faltan */
var Casa = /** @class */ (function (_super) {
    __extends(Casa, _super);
    function Casa(superficie, precio, numeroHabitaciones, numeroBaños, tipoCasa) {
        var _this = _super.call(this, precio, superficie) || this;
        _this.numeroHabitaciones = 1;
        _this.numeroBaños = 1;
        _this.tipoCasa = tipoCasa;
        _this.numeroHabitaciones = numeroHabitaciones;
        _this.numeroBaños = numeroBaños;
        return _this;
    }
    return Casa;
}(Propiedad));
/** Crear las personas y casas que se desee y hacer pruebas
 * (se valorará que se creen nuevas pruebas inventadas) */
/**
 * Este es un ejemplo de como debería funcionar el programa
 * una vez haya sido terminado, los comentarios a la derecha
 * de cada línea de código describen el resultado que se debe
 * mostrar al usuario por consola:
 */
var juan = new Persona("Juan", 32, 50000, "54672398L", EstadoCivil.SOLTERO);
var maria = new Persona("María", 34, 125000, "34568910T", EstadoCivil.SOLTERO);
var paula = new Persona("Paula", 27, 195000, "34589921D", EstadoCivil.SOLTERO);
var carlos = new Persona("Carlos", 35, 100000, "12341231Y", EstadoCivil.SOLTERO);
var laura = new Persona("Laura", 17, 100000, "12341231Y", EstadoCivil.SOLTERO);
var chalet1 = new Casa(152, 160000, 3, 2, TipoCasa.CHALET);
var piso1 = new Casa(68, 60000, 2, 1, TipoCasa.PISO);
var piso2 = new Casa(68, 30000, 2, 1, TipoCasa.PISO);
console.log(">Prueba: Matrimonio maria y juan: (expected ok) ");
maria.casarse(juan); // Debería funcionar correctamente.
console.log();
console.log(">Prueba: Matrimonio maria y paula: (expected error, maria ya esta casada)");
maria.casarse(paula); // Debería imprimir en consola el error "ERROR: La persona ya está casada".
console.log();
console.log(">Prueba: Matrimonio carlos y laura: (expected error, laura es menor de edad)");
carlos.casarse(laura); //No pueden casarse, laura es menor de edad
console.log();
laura.edad++; //pasa un año y laura ya se puede casar
console.log(">Prueba: Matrimonio carlos y laura: (expected ok)");
laura.casarse(carlos);
console.log();
console.log(">Prueba: Compra chalet: (expected ok)");
console.log("Dinero antes: ", juan.dinero + maria.dinero);
chalet1.comprar([juan, maria]); // Debería comprar el chalet correctamente al tener entre los dos suficiente dinero
console.log("Dinero despues: ", juan.dinero + maria.dinero);
console.log();
console.log(">Prueba: Compra piso: (expected error, no suficiente dinero)");
console.log("Dinero antes: ", juan.dinero);
piso1.comprar([juan]); // ERROR: Los compradores no tienen suficiente dinero para adquirir esta casa.
console.log("Dinero despues: ", juan.dinero);
console.log();
console.log(">Prueba: Compra chalet: (expected error, no esta en venta)");
console.log("Dinero antes: ", carlos.dinero);
chalet1.comprar([carlos]); // ERROR: el chalet no esta en venta
console.log("Dinero despues: ", carlos.dinero);
console.log();
console.log(">Prueba: Compra piso: (expected error, no esta en venta)");
console.log("Dinero antes: ", carlos.dinero);
piso2.comprar([carlos]); // ERROR: el chalet no esta en venta
console.log("Dinero despues: ", carlos.dinero);
console.log();
// Carlos pone a la venta el piso y se lo compra
piso2.enVenta = true;
console.log(">Prueba: Compra piso: (expected ok)");
console.log("Dinero antes vendedor:", carlos.dinero, "; comprador", laura.dinero);
piso2.comprar([laura]); // se vende!
console.log("Dinero despues vendedor:", carlos.dinero, "; comprador", laura.dinero);
console.log();
console.log(juan.estadoCivil); // casado
console.log(paula.estadoCivil); // soltero
console.log(chalet1.enVenta); // false
console.log(piso1.enVenta); //true
