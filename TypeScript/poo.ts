/** Crear interfaces y clases para definir esta situación:

Tenemos una ciudad con varias CASAS , estas pueden ser compradas por distintas PERSONAS.
 De las casas necesitamos guardar la información de su superficie, precio, 
 número de habitaciones, número de baños, tipo de casa (chalet, piso o duplex),
 si la casa está en venta, y su propietario/s en caso de que los tenga.
 De las personas necesitamos conocer su nombre, su edad, el dinero del que 
 disponen, su DNI y su estado civil,
 en caso de que las personas estén casadas además necesitamos conocer su pareja.

Habrá 2 acciones que sea posible realizar: 

- Comprar una casa: En caso de que una casa sea adquirida deberá dejar 
de estar en venta y se deberá registrar su propietario o propietarios.
- Casarse: 2 personas podrán casarse si no están ya casados con otra 
persona. Se deberá registrar su nueva pareja y cambiar su estado civil.

Se deberán crear las clases e interfaces necesarias para poder definir 
las distintas personas y casas.
Además las clases deberán contener los métodos necesarios para que se 
puedan realizar las acciones descritas.
Se deberá intentar utilizar tipos personalizados y herencia de clases. */
/** Enumeración con los tipos de casa, completar */

enum TipoCasa {
  CHALET = "chalet",
  PISO = "piso",
  DUPLEX = "duplex",
}

/** Enumeración con los tipos de estado civil, completar */
enum EstadoCivil {
  CASADO = "casado",
  SOLTERO = "soltero",
}

interface IPropiedad {
  precio: number
  propietarios: Persona[]
  enVenta: boolean
  superficie: number
}

class Propiedad implements IPropiedad {
  precio: number
  propietarios: Persona[]
  enVenta: boolean
  superficie: number

  constructor(precio: number, superficie: number) {
    this.precio = precio
    this.superficie = superficie
    this.enVenta = true
    this.propietarios = []
  }

  comprar(compradores: Persona[]) {
    // compruebo que la casa este en venta
    if (!this.enVenta) {
      console.log("ERROR: No esta en venta")
      return
    }

    // calculo capital de los compradores
    const capitalCompradores: number = compradores.reduce(
      (a, b) => b.dinero + a,
      0
    )

    // compruebo que tengan suficiente capital para la compra
    const isBudget = capitalCompradores >= this.precio

    if (!isBudget) {
      console.log("ERROR: Fondos insuficientes")
      return
    }

    // Almaceno los antiguos propietarios
    const antiguosPropietarios = [...this.propietarios]

    // Se actualizan propietarios
    this.propietarios = [...compradores]

    // La casa deja de estar en venta.
    this.enVenta = false

    // Genero un mensaje de respuesta
    const nombresCompradores: string[] = compradores.map((ele) => ele.nombre)
    console.log(`${nombresCompradores.join(", ")} han comprado una propiedad`)

    // Cobro el dinero a los nuevos propietarios
    let deuda: number = this.precio

    compradores.forEach((comprador) => {
      // Por desgracia voy dejando sin dinero por orden
      // a los propietarios hasta que la deuda esta pagada
      if (comprador.dinero <= deuda) {
        deuda -= comprador.dinero
        comprador.dinero = 0
      } else {
        comprador.dinero -= deuda
        deuda = 0
      }
    })

    // El primero antiguo propietario recibe todo el dinero de la compra
    // ya luego hace cuentas con los demas
    if (antiguosPropietarios.length > 0)
      antiguosPropietarios[0].dinero += this.precio
  }
}

/** Interfaz persona completar con el resto de información necesaria */
interface IPersona {
  nombre: string
  edad: number
  dinero: number
  dni: string
  estadoCivil: EstadoCivil
  pareja?: Persona
}

/** Clase persona, completar con atributos y constructor */
class Persona implements IPersona {
  nombre: string
  edad: number
  dinero: number
  dni: string
  estadoCivil: EstadoCivil
  pareja?: Persona

  constructor(
    nombre: string,
    edad: number,
    dinero: number,
    dni: string,
    estadoCivil: EstadoCivil
  ) {
    this.nombre = nombre
    this.estadoCivil = estadoCivil
    this.edad = edad
    this.dinero = dinero
    this.dni = dni
  }

  /** Implementar lógica para actualizar el estado civil de ambas personas y su pareja
   * Se deberá comprobar que las 2 personas estén solteras antes de casarlos.
   */
  casarse(persona: Persona) {
    if (this.edad < 18 || persona.edad < 18) {
      // Comprobacion de mayoria de edad
      console.log("ERROR: ambas personas deben ser mayores de edad")
      return
    }
    if (this.estadoCivil !== "soltero" || persona.estadoCivil !== "soltero") {
      // Comprobacion de ambos solteros/as
      console.log(`ERROR: ambas personas deben estar solteras`)
      return
    }
    // Actualizo estado civil
    this.estadoCivil = EstadoCivil.CASADO
    persona.estadoCivil = EstadoCivil.CASADO

    // Actualizo referencia a pareja
    this.pareja = persona
    persona.pareja = this

    // Genero mensaje de respuesta
    console.log(`${this.nombre} y ${persona.nombre} se han casado`)
  }
}

/** Interfaz Casa, completar con el resto de información necesaria */
interface ICasa extends IPropiedad {
  tipoCasa: TipoCasa
  numeroHabitaciones?: number
  numeroBaños?: number
}
/** Clase casa, completar con atributos que faltan */
class Casa extends Propiedad implements ICasa {
  tipoCasa: TipoCasa
  numeroHabitaciones: number = 1
  numeroBaños: number = 1

  constructor(
    superficie: number,
    precio: number,
    numeroHabitaciones: number,
    numeroBaños: number,
    tipoCasa: TipoCasa
  ) {
    super(precio, superficie)
    this.tipoCasa = tipoCasa
    this.numeroHabitaciones = numeroHabitaciones
    this.numeroBaños = numeroBaños
  }
}

/** Crear las personas y casas que se desee y hacer pruebas
 * (se valorará que se creen nuevas pruebas inventadas) */
/**
 * Este es un ejemplo de como debería funcionar el programa
 * una vez haya sido terminado, los comentarios a la derecha
 * de cada línea de código describen el resultado que se debe
 * mostrar al usuario por consola:
 */
const juan: Persona = new Persona(
  "Juan",
  32,
  50000,
  "54672398L",
  EstadoCivil.SOLTERO
)

const maria: Persona = new Persona(
  "María",
  34,
  125000,
  "34568910T",
  EstadoCivil.SOLTERO
)

const paula: Persona = new Persona(
  "Paula",
  27,
  195000,
  "34589921D",
  EstadoCivil.SOLTERO
)

const carlos: Persona = new Persona(
  "Carlos",
  35,
  100000,
  "12341231Y",
  EstadoCivil.SOLTERO
)

const laura: Persona = new Persona(
  "Laura",
  17,
  100000,
  "12341231Y",
  EstadoCivil.SOLTERO
)
const chalet1: Casa = new Casa(152, 160000, 3, 2, TipoCasa.CHALET)
const piso1: Casa = new Casa(68, 60000, 2, 1, TipoCasa.PISO)
const piso2: Casa = new Casa(68, 30000, 2, 1, TipoCasa.PISO)

console.log(">Prueba: Matrimonio maria y juan: (expected ok) ")
maria.casarse(juan) // Debería funcionar correctamente.
console.log()
console.log(
  ">Prueba: Matrimonio maria y paula: (expected error, maria ya esta casada)"
)
maria.casarse(paula) // Debería imprimir en consola el error "ERROR: La persona ya está casada".
console.log()

console.log(
  ">Prueba: Matrimonio carlos y laura: (expected error, laura es menor de edad)"
)
carlos.casarse(laura) //No pueden casarse, laura es menor de edad
console.log()

laura.edad++ //pasa un año y laura ya se puede casar
console.log(">Prueba: Matrimonio carlos y laura: (expected ok)")
laura.casarse(carlos)
console.log()

console.log(">Prueba: Compra chalet: (expected ok)")
console.log("Dinero antes: ", juan.dinero + maria.dinero)
chalet1.comprar([juan, maria]) // Debería comprar el chalet correctamente al tener entre los dos suficiente dinero
console.log("Dinero despues: ", juan.dinero + maria.dinero)
console.log()

console.log(">Prueba: Compra piso: (expected error, no suficiente dinero)")
console.log("Dinero antes: ", juan.dinero)
piso1.comprar([juan]) // ERROR: Los compradores no tienen suficiente dinero para adquirir esta casa.
console.log("Dinero despues: ", juan.dinero)
console.log()

console.log(">Prueba: Compra chalet: (expected error, no esta en venta)")
console.log("Dinero antes: ", carlos.dinero)
chalet1.comprar([carlos]) // ERROR: el chalet no esta en venta
console.log("Dinero despues: ", carlos.dinero)
console.log()

console.log(">Prueba: Compra piso: (expected error, no esta en venta)")
console.log("Dinero antes: ", carlos.dinero)
piso2.comprar([carlos]) // ERROR: el chalet no esta en venta
console.log("Dinero despues: ", carlos.dinero)
console.log()

// Carlos pone a la venta el piso y se lo compra
piso2.enVenta = true
console.log(">Prueba: Compra piso: (expected ok)")
console.log(
  "Dinero antes vendedor:",
  carlos.dinero,
  "; comprador",
  laura.dinero
)
piso2.comprar([laura]) // se vende!
console.log(
  "Dinero despues vendedor:",
  carlos.dinero,
  "; comprador",
  laura.dinero
)
console.log()

console.log(juan.estadoCivil) // casado
console.log(paula.estadoCivil) // soltero
console.log(chalet1.enVenta) // false
console.log(piso1.enVenta) //true
