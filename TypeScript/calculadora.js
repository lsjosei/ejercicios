// Creamos una enumeración para limitar las operaciones existentes en nuestra calculadora.
var Operacion;
(function (Operacion) {
    Operacion["SUMA"] = "+";
    Operacion["RESTA"] = "-";
    Operacion["DIVISION"] = "/";
    Operacion["MULTIPLICACION"] = "*";
    Operacion["POTENCIA"] = "^";
})(Operacion || (Operacion = {}));
var operando1 = parseInt(process.argv[2]);
var operando2 = parseInt(process.argv[3]);
/** Casteo al tipo Operacion para indicarle que el valor que introduce el usuario por consola es de dicho tipo */
var operacion = process.argv[4];
/** Creamos una función que raliza la operación que le especifiquemos y devuelve el resultado.
 * Si la operació no existe devuelve -1
 */
var operar = function (operando1, operando2, operacion) {
    switch (operacion) {
        case Operacion.SUMA:
            return sumar(operando1, operando2);
        case Operacion.RESTA:
            return restar(operando1, operando2);
        case Operacion.DIVISION:
            if (operando2 === 0) {
                console.log("ERROR: División entre 0");
                return -1;
            }
            return dividir(operando1, operando2);
        case Operacion.MULTIPLICACION:
            return multiplicar(operando1, operando2);
        case Operacion.POTENCIA:
            return potencia(operando1, operando2);
        default:
            console.log("ERROR: La operación introducida no existe");
            return -1;
    }
};
/** Función que suma dos números */
var sumar = function (operando1, operando2) {
    return operando1 + operando2;
};
var restar = function (operando1, operando2) {
    return operando1 - operando2;
};
var dividir = function (operando1, operando2) {
    return operando1 / operando2;
};
var multiplicar = function (operando1, operando2) {
    return operando1 * operando2;
};
var potencia = function (operando1, operando2) {
    return Math.pow(operando1, operando2);
};
/** Se infiere el tipo number de la función al ser el tipo de su valor de retorno */
var result = operar(operando1, operando2, operacion);
/** Imprimimos el resultado de nuestra operación */
console.log(result);
