const streamers = [{name: 'Rubius', age: 32, gameMorePlayed: 'Minecraft'}, {name: 'Ibai', age: 25, gameMorePlayed: 'League of Legends'}, {name: 'Reven', age: 43, gameMorePlayed: 'League of Legends'}, {name: 'AuronPlay', age: 33, gameMorePlayed: 'Among Us'}];

document.querySelector("input")
.addEventListener("input",(e)=> {
    console.log(streamers
        .filter(ele => ele.name.toLocaleLowerCase().includes(e.target.value)))
})