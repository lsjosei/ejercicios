const animalFunction = () => {
    return {name: 'Bengal Tiger', race: 'Tiger'}
};

let {name: animalName, race: animalRace} = {...animalFunction()}

console.log(animalName, animalRace)