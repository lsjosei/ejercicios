const baseUrl = "https://api.nationalize.io"
let url = baseUrl + `?name=${"jose"}`
console.log(url)

document.querySelector("button").addEventListener("click", (e) => {
  let queryName = document.querySelector("input").value
  fetch(baseUrl + `?name=${queryName}`)
    .then((r) => r.json())
    .then((data) => {
      let message = `El nombre '${queryName}' tiene`
      data.country.forEach((ele, index) => {
        let conector
        switch (index) {
          case 0:
            conector = ""
            break
          case data.country.length - 1:
            conector = " y"
            break
          default:
            conector = ","
        }
        message += `${conector} un ${ele.probability}% de ser de ${ele.country_id}`

      })

      let pEle = document.createElement("p")
      pEle.innerHTML = message
      document.body.appendChild(pEle)

    })
    document.querySelector("input").value = ""
})
