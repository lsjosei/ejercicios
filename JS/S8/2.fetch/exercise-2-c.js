const baseUrl = "https://api.nationalize.io"
let url = baseUrl + `?name=${"jose"}`
console.log(url)

document.querySelector("button").addEventListener("click", (e) => {
  let queryName = document.querySelector("input").value
  fetch(baseUrl + `?name=${queryName}`)
    .then((r) => r.json())
    .then((data) => {
      let message = `El nombre '${queryName}' tiene`
      data.country.forEach((ele, index) => {
        let conector
        switch (index) {
          case 0:
            conector = ""
            break
          case data.country.length - 1:
            conector = " y"
            break
          default:
            conector = ","
        }
        message += `${conector} un ${ele.probability}% de ser de ${ele.country_id}`
      })

      let divEle = document.createElement("div")
      let pEle = document.createElement("p")

      let btnEle = document.createElement("button")
      btnEle.innerHTML = "X"
      btnEle.addEventListener("click", (e) => {
        e.target.parentElement.remove()
      })
      pEle.innerHTML += message
      pEle.style.display = "inline-block"
      divEle.appendChild(btnEle)
      divEle.appendChild(pEle)
      document.body.appendChild(divEle)
    })
  document.querySelector("input").value = ""
})
