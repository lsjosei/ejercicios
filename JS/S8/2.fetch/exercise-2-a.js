const baseUrl = 'https://api.nationalize.io';


document.querySelector("button")
.addEventListener("click",(e)=> {
    let queryName = document.querySelector("input").value
    fetch(baseUrl+`?name=${queryName}`)
    .then(r=>r.json())
    .then(data=>document.querySelector("#result").innerHTML= JSON.stringify(data))
})