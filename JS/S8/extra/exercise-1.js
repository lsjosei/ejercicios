fetch('https://breakingbadapi.com/api/characters')
.then(res => res.json())
.then(data => {
    console.log(data)
    data.forEach(ele => {
        divEle = document.createElement("div")
        imgEle = document.createElement("img")
        pEle = document.createElement("p")
        imgEle.src = ele.img
        pEle.innerHTML = ele.name

        divEle.appendChild(imgEle)
        divEle.appendChild(pEle)

        document.body.style.display = "flex"
        document.body.style.flexWrap = "wrap"

        divEle.style.display="flex"
        divEle.style.justifyContent="center"
        divEle.style.flexDirection = "column"
        divEle.style.margin = "2rem 2rem"

        divEle.style.textAlign = "center"

        divEle.style.width = "300px"
        imgEle.style.width = "100%"

        document.body.appendChild(divEle)
    })
})