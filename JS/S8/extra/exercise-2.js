fetch("http://localhost:3000/diary")
.then(res => res.json())
.then(data => {
    data.sort((a,b) => {
        return new Date(a.date) - new Date(b.date)
    })
    .forEach(ele => {
        let divEle = document.createElement("div")
        let h3Ele = document.createElement("h3")
        let h5Ele = document.createElement("h5")
        let pEle = document.createElement("p")
        let btnEle = document.createElement("button")

        h3Ele.innerHTML = ele.title
        h5Ele.innerHTML = ele.date
        pEle.innerHTML = ele.description
        btnEle.innerHTML = "Eliminar"
        btnEle.addEventListener("click",(e)=>{
            e.target.parentElement.remove()
        })

        divEle.appendChild(h3Ele)
        divEle.appendChild(h5Ele)
        divEle.appendChild(pEle)
        divEle.appendChild(btnEle)

        document.body.appendChild(divEle)
    })
})