const shuffletArr = (arr) => {
  return arr
    .map((value) => ({ value, sort: Math.random() }))
    .sort((a, b) => a.sort - b.sort)
    .map(({ value }) => value)
}

const gameboard = document.querySelector(".gameboard")
let questions = []

document.querySelector("#btnStart").addEventListener("click", (e) => {
  let numberOfQuestions = document.querySelector("#fNumberQuestions").value
  e.target.innerHTML = "New game"
  fetch(`https://opentdb.com/api.php?amount=${numberOfQuestions}`)
    .then((res) => res.json())
    .then((data) => {
      gameboard.innerHTML = ""
      questions = data.results
      data.results.forEach((questionData, index) => {
        let questionContainer = document.createElement("div")

        let question = document.createElement("p")
        question.innerHTML = questionData.question

        let answers = document.createElement("select")
        answers.dataset.questionNumber = index

        let allAnswers = shuffletArr([
          ...questionData.incorrect_answers,
          questionData.correct_answer,
        ])

        allAnswers.forEach((ele) => {
          let option = document.createElement("option")

          option.innerHTML = ele
          option.value = ele
          answers.append(option)
        })

        questionContainer.appendChild(question)
        questionContainer.appendChild(answers)

        gameboard.appendChild(questionContainer)
        document.querySelector("#btnCheck").style.display = "inline-block"
      })
    })
})

document.querySelector("#btnCheck").addEventListener("click", (e) => {
  if (questions.length < 1) return
  e.target.style.display = "none"
  let points = [0, 0]
  questions.forEach((question, index) => {
    points[1]++
    let optionInput = document.querySelector(
      `select[data-question-number='${index}']`
    )

    let parentDiv = optionInput.parentElement
    let userAnswer = optionInput.value

    optionInput.remove()

    let confirmation = document.createElement("p")

    if (userAnswer === question.correct_answer) {
      parentDiv.style.backgroundColor = "lightgreen"
      confirmation.innerHTML = `Correct! '${userAnswer}' is the correct answer`
      points[0]++
    } else {
      parentDiv.style.backgroundColor = "indianred"
      confirmation.innerHTML = `Sorry, the right answer is '${question.correct_answer}'`
    }

    parentDiv.appendChild(confirmation)
  })
  let result = document.createElement("p")
  result.innerHTML = `Your score is ${points[0]} corret answers out of ${points[1]}`
  gameboard.appendChild(result)
})
