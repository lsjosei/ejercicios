fetch(`http://localhost:3000/planets`)
  .then((res) => res.json())
  .then((data) => {
    console.log(data)
    // genero un ul con los planetas, cada planeta tendra un p con su nombre,
    // y debajo de este se hara append a la lista de pjs
    let planetUl = document.createElement("ul")
    data.forEach((planeta) => {
      //cada li es un planeta
      //contiene un p con el nombre, el p es clickable y despliega a los pjs
      let planetLi = document.createElement("li")
      let planetNameP = document.createElement("p")
      planetNameP.innerHTML = planeta.name
      planetNameP.classList.add("clickable")
      planetNameP.classList.add("name-label")
      planetNameP.style.before
      planetLi.appendChild(planetNameP)
      planetUl.appendChild(planetLi)

      planetNameP.addEventListener("click", (e) => {
        let isShown = [
          ...e.target.parentElement.children[1].classList,
        ].includes("character-list--hidden")

        ;[...document.querySelectorAll(".character-list")].forEach((ele) =>
          ele.classList.add("character-list--hidden")
        )
        if (isShown)
          e.target.parentElement.children[1].classList.remove(
            "character-list--hidden"
          )
      })

      displayChrs(planetLi, planeta.id)
    })
    document.body.appendChild(planetUl)
  })

let displayChrs = (parent, planetId) => {
  //cada listado de pjs va dentro de un div
  //este div contiene al filter y un ul con los pjs
  //cada li de pj contiene un p con su nombre y una img con su img
  let charactersDiv = document.createElement("div")
  charactersDiv.classList.add("character-list")
  //inicializo las listas escondidas y las muestro en funcion de los eventos
  //de click
  charactersDiv.classList.add("character-list--hidden")

  let inputEle = document.createElement("input")
  inputEle.placeholder = "Escribe para filtrar"
  inputEle.classList.add("filter")
  inputEle.dataset.planetId = planetId

  fetch(`http://localhost:3000/characters?idPlanet=${planetId}`)
    .then((res) => res.json())
    .then((characterData) => {
      let chrList = document.createElement("ul")
      chrList.classList.add("characters")

      characterData.forEach((chr) => {
        let chrLi = document.createElement("li")
        chrLi.classList.add(`chr`)
        //esta clase me permite luego filtrar
        chrLi.classList.add(`chr--${planetId}`)
        let chrName = document.createElement("p")
        chrName.innerHTML = chr.name
        let chrImg = document.createElement("img")
        chrImg.src = chr.avatar

        chrLi.appendChild(chrName)
        chrLi.appendChild(chrImg)

        chrList.appendChild(chrLi)
      })

      charactersDiv.appendChild(inputEle)
      charactersDiv.appendChild(chrList)
      parent.appendChild(charactersDiv)

      inputEle.addEventListener("input", (e) => {
        //uso el valor del input cuando se modifica
        //para filtrar la lista de pjs en el planeta correspondiente
        //
        let filterValue = e.target.value.toLowerCase()
        let chrsInPlanet = [
          ...document.querySelectorAll(`.chr--${planetId}
          `),
        ]
        chrsInPlanet.forEach((chr) => {
          chr.classList.add("chr--hidden")
          let isEmpty = filterValue === ""
          let isIncluded = chr.children[0].innerHTML
            .toLowerCase()
            .includes(filterValue)
          if (isEmpty || isIncluded) {
            chr.classList.remove("chr--hidden")
          }
        })
      })
    })
}
