let pjData
let chr1,
  chr2 = [undefined, undefined]

let fase = "selection"

let chrCarousel = document.querySelector(".chr-carousel")
let battleground = document.querySelector(".battleground")

fetch("http://localhost:3000/characters")
  .then((res) => res.json())
  .then((data) => {
    pjData = data
  })
  .then(() => pjData.forEach((chrData) => displayCharacter(chrData, chrCarousel)))

document.querySelector(".fight").addEventListener("click", (e) => {
  e.target.classList.add("hidden")
  battleground.classList.remove("hidden")

  //obtengo los pjs seleccionados
  let [chr1Ele, chr2Ele] = [
    ...document.querySelectorAll(".chr-carousel__item--selected"),
  ]

  //randomizo el orden de la batalla
  if (Math.random() > 0.5) [chr1Ele, chr2Ele] = [chr2Ele, chr1Ele]

  //obtengo los datos de los pjs
  chr1 = pjData.filter((ele) => ele.id == chr1Ele.dataset.id)[0]
  chr2 = pjData.filter((ele) => ele.id == chr2Ele.dataset.id)[0]

  chr1.tempVit = chr1.vitality
  chr2.tempVit = chr2.vitality

  //inicializo fasde de combate
  fase = "combat"

  //escondo al resto de pjs
  let allChrs = [...document.querySelector(".chr-carousel").children]

  allChrs.forEach((ele) => {
    if (![...ele.classList].includes("chr-carousel__item--selected")) {
      ele.classList.add("hidden")
    }
  })

  combatRound(chr1, chr2)
})

//funcion de ronda de combate (tecnicamente turnos)
//es una funcion recursiva que se llama a si misma al acabar si no ha habido derrota
//entre rondas hay una pequeña pausa que aseguro mediante una promesa
let combatRound = (chr1, chr2, wait = true) => {
  let p = new Promise((resolve, reject) => {
    let dmg = calculateDamage(chr1.damage)

    //me aseguro que no se puedan curar de un hachazo
    let realDmg = Math.max(0, dmg - chr2.defense)
    chr2.tempVit -= realDmg

    let p = document.createElement("p")
    battleground.insertBefore(p, battleground.firstChild)
    p.innerHTML = `${chr1.name} has dealt ${realDmg} damage to ${chr2.name}, lowering its vitality stat down to ${chr2.tempVit}`

    resolve([chr2, chr1, !wait])
  })

  p.then((data) => {
    if (chr2.tempVit > 0) {
      if (data[2]) {
        setTimeout(() => {
          combatRound(data[0], data[1], data[2])
        }, 1500)
      } else {
        combatRound(data[0], data[1], data[2])
        let hr = document.createElement("hr")
        battleground.insertBefore(hr, battleground.firstChild)
      }
    } else {
      let h3 = document.createElement("h3")
      battleground.insertBefore(h3, battleground.firstChild)
      h3.innerHTML = `${chr2.name} is defeated! ${chr1.name} wins the fight`

      let btnReset = document.createElement("button")
      battleground.insertBefore(btnReset, battleground.firstChild)
      btnReset.id = "reset-fight"
      btnReset.innerHTML = "New Fight"

      btnReset.addEventListener("click", (e) => {
        battleground.innerHTML = ""
        chr1, (chr2 = [undefined, undefined])
        let allChrs = [...document.querySelector(".chr-carousel").children]
        allChrs.forEach((ele) => {
          ele.classList.remove("chr-carousel__item--selected")
          ele.classList.remove("hidden")
        })

        fase = "selection"

        e.target.remove()
      })
    }
  })
}

let displayCharacter = (chrData, container) => {
  // muestro cada pjs con su nombre en el carousel
  let pjContainer = document.createElement("div")
  pjContainer.classList.add("chr-carousel__item")
  pjContainer.dataset.id = chrData.id

  let pjName = document.createElement("p")
  let pjImg = document.createElement("img")

  pjName.innerHTML = chrData.name
  pjImg.src = chrData.avatar
  pjStats = generateStatTable(chrData)

  pjContainer.appendChild(pjName)
  pjContainer.appendChild(pjStats)
  pjContainer.appendChild(pjImg)

  //gestiono el click en el pj para seleccionarlo
  // limitando la seleccion a 2
  //lo hago mediante las clases de css que asigno

  pjContainer.addEventListener("click", (e) => {
    if (!(fase === "selection")) return
    let isSelected = [...pjContainer.classList].includes(
      "chr-carousel__item--selected"
    )

    if (isSelected) {
      pjContainer.classList.remove("chr-carousel__item--selected")
      document.querySelector(".fight").classList.add("hidden")
      return
    }

    let numberSelectedChrs = [
      ...document.querySelectorAll(".chr-carousel__item"),
    ].filter((ele) =>
      [...ele.classList].includes("chr-carousel__item--selected")
    ).length

    if (numberSelectedChrs < 2) {
      pjContainer.classList.add("chr-carousel__item--selected")
      if (numberSelectedChrs === 1) {
        document.querySelector(".fight").classList.remove("hidden")
      }
    }
  })
  chrCarousel.appendChild(pjContainer)
}

//funcion para generar la mini tabla de stats dinamicamente
let generateStatTable = (chrData) => {
  let pjStats = document.createElement("table")

  let tableHeader = document.createElement("tr")
  let tdH1 = document.createElement("td")
  let tdH2 = document.createElement("td")

  tdH1.innerHTML = "Attr"
  tdH2.innerHTML = "Value"

  tableHeader.appendChild(tdH1)
  tableHeader.appendChild(tdH2)
  pjStats.appendChild(tableHeader)

  let vitRow = document.createElement("tr")
  let tdR11 = document.createElement("td")
  let tdR12 = document.createElement("td")

  tdR11.innerHTML = "Vit"
  tdR12.innerHTML = chrData.vitality

  vitRow.appendChild(tdR11)
  vitRow.appendChild(tdR12)
  pjStats.appendChild(vitRow)

  let dmgRow = document.createElement("tr")
  let tdR21 = document.createElement("td")
  let tdR22 = document.createElement("td")

  tdR21.innerHTML = "Dmg"
  tdR22.innerHTML = chrData.damage

  dmgRow.appendChild(tdR21)
  dmgRow.appendChild(tdR22)
  pjStats.appendChild(dmgRow)

  let defRow = document.createElement("tr")
  let tdR31 = document.createElement("td")
  let tdR32 = document.createElement("td")

  tdR31.innerHTML = "Def"
  tdR32.innerHTML = chrData.defense

  defRow.appendChild(tdR31)
  defRow.appendChild(tdR32)
  pjStats.appendChild(defRow)

  return pjStats
}

function calculateDamage(dices, critic = 1000) {
  return dices
    .map((ele) => {
      let [count, sides] = ele.split("d")
      let sum = 0
      for (let i = 0; i < count; i++) {
        let roll = Math.floor(sides * Math.random()) + 1
        sum += roll == critic ? roll * 2 : roll
      }
      return sum
    })
    .reduce((a, b) => a + b)
}
