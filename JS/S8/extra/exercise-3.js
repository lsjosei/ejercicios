fetch("http://localhost:3000/orders")
  .then((res) => res.json())
  .then((data) => {
    let mainUlEle = document.createElement("ul")
    data
      .sort((a, b) => {
        return new Date(b.date) - new Date(a.date)
      })
      .forEach((pedido) => {
        let minLiEle = document.createElement("li")
        minLiEle.innerHTML = `Pedido número ${pedido.id} (${pedido.date})`

        let ulEle = document.createElement("ul")

        mainUlEle.appendChild(minLiEle)
        pedido.products.forEach((product) => {
          fetch(`http://localhost:3000/products/${product.productId}`)
            .then((res) => res.json())
            .then((productData) => {
              console.log(productData)
              let liEle = document.createElement("li")
              liEle.innerHTML = `${productData.name}: ${product.quantity}`
              ulEle.appendChild(liEle)
            })
        })
        mainUlEle.appendChild(ulEle)
      })

    document.body.appendChild(mainUlEle)

    console.log(data)
  })
