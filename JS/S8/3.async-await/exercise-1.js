const runTimeOut = () => {
  const promise = new Promise((resolve) => {
    setTimeout(function () {
      resolve()
    }, 2000)
  })

  promise.then(() => {
    console.log("Time out!")
  })
}

// runTimeOut()

let runTimeOut2 = async () => {
  await setTimeout(() => console.log("Time out! (but the other)"), 2000)
}

runTimeOut2()
