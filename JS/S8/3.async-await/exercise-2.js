function getCharacters () {
    fetch('https://rickandmortyapi.com/api/character').then(res => res.json()).then(characters => console.log(characters));
}

getCharacters();

let getCharacters2 = async () => {
    let response = await fetch('https://rickandmortyapi.com/api/character')
    let characters = await response.json()
    console.log(characters)
}

getCharacters2()