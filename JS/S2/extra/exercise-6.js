let swap = (arr, i1, i2) => {
    [arr[i1], arr[i2]] = [arr[i2], arr[i1]]
    return arr
}

console.log(swap(
    ['Mesirve', 'Cristiano Romualdo', 'Fernando Muralla', 'Ronalguiño'],
    1,2
))