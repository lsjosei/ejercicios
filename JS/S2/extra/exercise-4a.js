let findArrayIndex = (arr, text) => {
    return arr.indexOf(text)
}

console.log(findArrayIndex(
    ["hey","que","tal"],
    "que"
))

console.log(findArrayIndex(
    ['Caracol', 'Mosquito', 'Salamandra', 'Ajolote'],
    "Salamandra"
))