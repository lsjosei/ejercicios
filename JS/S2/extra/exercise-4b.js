let findArrayIndex = (arr, text) => {
    return arr.indexOf(text)
}

let removeItem = (arr, text) => {
    let index = findArrayIndex(arr, text)
    arr.splice(index,1)
    return arr
}


console.log(removeItem(
    ['Caracol', 'Mosquito', 'Salamandra', 'Ajolote'],
    "Salamandra"
))
console.log(removeItem(
    ['Caracol', 'Mosquito', 'Salamandra', 'Ajolote'],
    "Caracol"
))
console.log(removeItem(
    ['Caracol', 'Mosquito', 'Salamandra', 'Ajolote'],
    "Mosquito"
))