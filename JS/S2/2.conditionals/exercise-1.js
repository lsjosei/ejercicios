const alumns = [
    {name: 'Pepe Viruela', T1: false, T2: false, T3: true}, {name: 'Lucia Aranda', T1: true, T2: false, T3: true}, {name: 'Abel Cabeza', T1: false, T2: true, T3: true}, {name: 'Alfredo Blanco', T1: false, T2: false, T3: false}, {name: 'Raquel Benito', T1: true, T2: true, T3: true}
]

alumns.forEach(ele=>{
    ele.isApproved = false
    if ((ele.T1 || ele.T2) && (ele.T1 || ele.T3) && (ele.T2 || ele.T3)) {
        ele.isApproved = true
    }
})

console.log(alumns)