const placesToTravel = [{id: 5, name: 'Japan'}, {id: 11, name: 'Venecia'}, {id: 23, name: 'Murcia'}, {id: 40, name: 'Santander'}, {id: 44, name: 'Filipinas'}, {id: 59, name: 'Madagascar'}]

let idsToDel = [11, 40]

for (let i = placesToTravel.length-1; i >= 0; i--) {
    if (idsToDel.includes(placesToTravel[i].id)) placesToTravel.splice(i,1)
}

console.log(placesToTravel)