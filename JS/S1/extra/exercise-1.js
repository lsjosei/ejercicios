const users = [{name: "Abel", years: 43}, {name: "Maria", years: 18}, {name: "Pedro", years: 14}, {name: "Samantha", years: 32}, {name: "Raquel", years: 16}];

users.forEach(ele => {
    console.log(`${ele.years >= 18 ? "Usuarios mayores de edad" : "Usuarios menores de edad"}: ${ele.name}`)
})