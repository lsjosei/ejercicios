const movies = [{name: "Your Name", durationInMinutes: 130},{name: "Pesadilla antes de navidad", durationInMinutes: 225}, {name: "Origen", durationInMinutes: 165}, {name: "El señor de los anillos", durationInMinutes: 967}, {name: "Solo en casa", durationInMinutes: 214}, {name: "El jardin de las palabras", durationInMinutes: 40}];

let shortFilm = []
let mediumFilm = []
let longFilm = []

movies.forEach(ele => {
    if (ele.durationInMinutes > 200) longFilm.push(ele)
    if (ele.durationInMinutes <= 200 && ele.durationInMinutes > 100) mediumFilm.push(ele)
    if (ele.durationInMinutes <= 100) shortFilm.push(ele)
})


console.log(shortFilm)
console.log(mediumFilm)
console.log(longFilm)