const apps = ['Facebook', 'Netflix', 'Instagram', 'Snapchat', 'Twitter'];

let ulEle = document.createElement("ul")

apps.forEach(ele => {
    let liEle = document.createElement("li")
    liEle.innerHTML = ele
    ulEle.appendChild(liEle)
})

document.body.appendChild(ulEle)