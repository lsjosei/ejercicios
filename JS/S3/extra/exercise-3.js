const cars = ['Mazda 6', 'Ford fiesta', 'Audi A4', 'Toyota corola'];

let ulEle = document.createElement("ul")

cars.forEach(ele=>{
    let liEle = document.createElement("li")
    liEle.innerHTML = ele
    liEle.dataset.function="printHere"
    ulEle.appendChild(liEle)
})

document.body.appendChild(ulEle)