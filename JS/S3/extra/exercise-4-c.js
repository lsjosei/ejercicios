const countries = [{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=1'}, {title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=2'},{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=3'},{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=4'},{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=5'}];

countries.forEach(ele=>{
    let divEle = document.createElement("div")
    let h2Ele = document.createElement("h2")
    let imgEle = document.createElement("img")
    let btnEle = document.createElement("button")
    h2Ele.innerHTML = ele.title
    imgEle.src = ele.imgUrl
    btnEle.innerHTML = "Borrar"
    btnEle.addEventListener("click", (e) => {
        e.target.parentElement.remove()
    })
    divEle.appendChild(h2Ele)
    divEle.appendChild(imgEle)
    divEle.appendChild(btnEle)
    document.body.appendChild(divEle)
})

document.querySelector("button").addEventListener("click",(e) => {
    let divs = document.querySelectorAll("div")
    if (divs.length > 0) divs[divs.length - 1].remove()
})