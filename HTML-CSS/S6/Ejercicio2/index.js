let progressBar = document.querySelector(".progress2")
let checkTick = document.querySelector(".tick2")

let reset = () => {
  progressBar.style.width = 0
  checkTick.style.color = "rgb(209, 233, 255)"
  load(5)
}

let load = (loadPercentaje = 0) => {
  let p = new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve()
    }, 100)
  }).then(() => {
    //me aseguro que no puedo pasar de 100 para no crear
    //un bucle infinito
    loadPercentaje = Math.min(loadPercentaje, 100)

    //ajusto la barra de carga
    progressBar.style.width = (30 * loadPercentaje) / 100 + "rem"
    if (loadPercentaje >= 100) {
      //si la carga ha terminado  hago una comprobacion
      //de que los datos estan
      if (true) {
        setTimeout(() => {
          checkTick.style.color = "rgb(77,247,62)"

          setTimeout(() => {
            //reinicio el proceso
            reset()
          }, 1500)
        }, 500)
      }
      return
    }
    load(loadPercentaje + 5)
  })
}

reset()
